package me.flyray.admin.rpc.service;

import me.flyray.admin.entity.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.admin.biz.DeptBiz;

/** 
* @author: bolei
* @date：2018年4月16日 下午2:22:58 
* @description：类说明
*/

@Service
public class DeptService {

	@Autowired
    private DeptBiz deptBiz;
	
	/**
	 * 新增机构部门
	 * @param entity
	 */
	public void addDept(Dept entity) {
		deptBiz.insertSelective(entity);
    }

	public void updateByPlatformId(Dept dept) {
		deptBiz.updateByPlatformId(dept);
	}
}
