package me.flyray.admin.mapper;

import me.flyray.admin.entity.Role;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMapper extends Mapper<Role> {
	public Integer insertRole(Role role);
}