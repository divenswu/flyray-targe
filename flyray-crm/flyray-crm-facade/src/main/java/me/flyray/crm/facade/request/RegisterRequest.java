package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("注册请求参数")
public class RegisterRequest implements Serializable {

	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;

	@ApiModelProperty("手机号")
	private String phone;

	@ApiModelProperty("用户名")
	private String username;

	@NotNull(message="密码不能为空")
	@ApiModelProperty("密码")
	private String password;
	

//	@NotNull(message="验证码不能为空")
	@ApiModelProperty("验证码")
	private String smsCode;

	/**
	 * 商户类型
	 */
	@ApiModelProperty("商户类型")
	private String merchantType;

	@ApiModelProperty("客户类型")
	private String customerType;

	@ApiModelProperty("设备ID")
	private String deviceId;
	
	/**
	 * 设备类型：0-android、1-ios、2-PC
	 */
	@ApiModelProperty("设备类型")
	private String deviceType;
	
	@ApiModelProperty("密码盐值")
	private String passwordSalt;
	
}
