package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 会员登录请求报文
 * @author centerroot
 * @time 创建时间:2018年9月8日下午4:39:09
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("会员登录请求报文")
public class LoginRequest{

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;


	/**
	 * 手机号
	 */
	@ApiModelProperty("手机号")
	private String phone;

	/**
	 * 用户名
	 */
	@NotNull(message = "客户ID能为空")
	@ApiModelProperty("用户名")
	private String customerId;

	/**
	 * 用户名
	 */
	@ApiModelProperty("用户名")
	private String username;

	/**
	 * 登录密码
	 */
	@NotNull(message = "登录密码不能为空")
	@ApiModelProperty("登录密码")
	private String password;
	/**
	 * 角色类型
	 * 01:个人  02：商户
	 */
	@ApiModelProperty("角色类型")
	private String roleType;
	/**
	 * 图形验证码
	 */
	@ApiModelProperty("图形验证码")
	private String imgCode;
	/**
	 * APP版本
	 */
	@ApiModelProperty("APP版本")
	private String appVersion;
	/**
	 * 操作系统版本
	 */
	@ApiModelProperty("操作系统版本")
	private String osVersion;
	
}
