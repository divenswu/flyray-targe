package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("冻结接口参数")
public class FreezeRequest {
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty("个人客户编号")
	private String personalId;
	
	@NotNull(message = "订单号不能为空")
	@ApiModelProperty("订单号")
	private String orderNo;
	
//	@NotNull(message = "退款订单号不能为空")
//	@ApiModelProperty("退款订单号")
//	private String refundOrderNo;
	
	@NotNull(message = "冻结金额不能为空")
	@ApiModelProperty("冻结金额")
	private String freezeAmt;

	@ApiModelProperty("商户编号")
	private String merchantId;
	
	@ApiModelProperty("商户类型编号")
	private String merchantType;

	@NotNull(message = "客户编号不能为空")
	@ApiModelProperty("客户编号")
	private String customerId;
	
	@NotNull(message = "客户类型不能为空")
	@ApiModelProperty("客户类型")//01:商户 02:个人
	private String customerType;
	
	@NotNull(message = "账户类型不能为空")
	@ApiModelProperty("账户类型")
	private String accountType;
	
	@NotNull(message = "交易类型不能为空")
	@ApiModelProperty("交易类型")
	private String tradeType;
	
	
}
