package me.flyray.crm.core.modules.platform;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.crm.core.entity.PlatformSafetyConfig;
import me.flyray.crm.facade.request.SafetyConfigRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.platform.PlatformSafetyConfigBiz;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("platformSafetyConfig")
public class PlatformSafetyConfigController extends BaseController<PlatformSafetyConfigBiz, PlatformSafetyConfig> {

	
	/**
	 * 列表
	 * @param bean
	 * @return
	 */
	@RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<PlatformSafetyConfig> pageList(@RequestBody SafetyConfigRequest bean){
		bean.setPlatformId(setPlatformId(bean.getPlatformId()));
        return baseBiz.safetyConfigList(bean);
    }
	
	@RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> add(@RequestBody SafetyConfigRequest bean) throws Exception{
        baseBiz.addConfig(bean);
        return ResponseEntity.ok(bean);
    }
	
	@ApiOperation("修改平台/商户安全配置")
	@RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> update(@RequestBody @Valid SafetyConfigRequest safetyConfigRequest) throws Exception {
		Map<String, Object> response = baseBiz.updateConfig(safetyConfigRequest);
		return response;
    }
	
	@ApiOperation("删除平台/商户安全配置")
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> delete(@RequestBody @Valid SafetyConfigRequest safetyConfigRequest) throws Exception {
		Map<String, Object> response = baseBiz.deleteConfig(safetyConfigRequest);
		return response;
    }
}