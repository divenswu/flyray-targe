package me.flyray.crm.core.biz.platform;

import me.flyray.crm.core.mapper.PlatformCoinTransactionToMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;
import me.flyray.crm.core.entity.PlatformCoinTransactionTo;

/**
 * 
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@Service
public class PlatformCoinTransactionToBiz extends BaseBiz<PlatformCoinTransactionToMapper,PlatformCoinTransactionTo> {
}