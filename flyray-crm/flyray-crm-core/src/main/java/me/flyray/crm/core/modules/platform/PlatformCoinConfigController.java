package me.flyray.crm.core.modules.platform;

import me.flyray.crm.core.biz.platform.PlatformCoinConfigBiz;
import me.flyray.crm.core.entity.PlatformCoinConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import me.flyray.common.rest.BaseController;

@Controller
@RequestMapping("platformCoinConfig")
public class PlatformCoinConfigController extends BaseController<PlatformCoinConfigBiz, PlatformCoinConfig> {

}