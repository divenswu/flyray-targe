package me.flyray.crm.core.feignserver;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.crm.core.biz.platform.PlatformAccountExtendBiz;
import me.flyray.crm.facade.request.*;
import me.flyray.crm.facade.response.MerchantAccountOpJournalResponse;
import me.flyray.crm.facade.response.CustomerAccountQueryResponse;
import me.flyray.crm.facade.response.MerchantAccountOpBalanceResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/***
 * 运营平台相关的接口
 * */
@Api(tags="运营平台相关的接口")
@Controller
@RequestMapping("feign/platform")
public class FeignPlatformService {
	
	@Autowired
	private PlatformAccountExtendBiz platformAccountExtendBiz;
	

	/**
	 *  个人或者商户或者平台账户余额接口
	 * */
	@ApiOperation("个人或者商户或者平台账户余额接口")
	@RequestMapping(value = "/account_balance",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<MerchantAccountOpBalanceResp> accountBalance(@RequestBody @Valid MerchantBalanceOpRequest merchantBalanceOpRequest){
	    MerchantAccountOpBalanceResp merchantAccountOpBalanceResp = platformAccountExtendBiz.accountBalance(merchantBalanceOpRequest);
		return BaseApiResponse.newSuccess(merchantAccountOpBalanceResp);
    }

    /**
     *  个人或者商户或者平台账户余额接口
     * */
    @ApiOperation("个人或者商户或者平台账户余额接口")
    @RequestMapping(value = "/list_account",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<CustomerAccountQueryResponse> listAccount(@RequestBody @Valid MerchantAccountOpRequest merchantAccountOpRequest){
        return platformAccountExtendBiz.listAccount(merchantAccountOpRequest);
    }

    /**
     *  运营查询商户流水接口
     * */
    @ApiOperation("运营查询商户流水")
    @RequestMapping(value = "/list_journal",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<MerchantAccountOpJournalResponse> listJournal(@RequestBody @Valid MerchantAccountOpJournalRequest merchantAccountOpJournalRequest){
        return platformAccountExtendBiz.listJournal(merchantAccountOpJournalRequest);
    }
}
